# Задача №1
# Модифицируйте класс Point с занятия. Реализуйте передачу в X и Y только числовых значений.
# Модифицируйте класс Line с занятия следующим образом - обеспечьте проверку точек начала и конца
# - координаты точек начала и конца отрезка не должны совпадать.

class Point:
    x_coord = 0
    y_coord = 0

    def __init__(self, x: int, y: int):
        if isinstance(x, int) and isinstance(y, int):
            self.x_coord = x
            self.y_coord = y
        else:
            raise TypeError

    def __str__(self):
        return f'Point {self.x_coord}, {self.y_coord}'


point1 = Point(2, 4)
point2 = Point(3, 4)

res = str(point2)

print(res)
print(point1)
print(point2)


class Line:
    first_point = None
    second_point = None

    def __init__(self, p1, p2):
        if isinstance(p1, Point) and isinstance(p2, Point):
            self.first_point = p1
            self.second_point = p2
        else:
            raise Exception

        if self.first_point.x_coord == self.second_point.x_coord \
                and self.first_point.y_coord == self.second_point.y_coord:
            raise Exception

    def length(self):
        x = (self.first_point.x_coord - self.second_point.x_coord)**2
        y = (self.first_point.y_coord - self.second_point.y_coord)**2
        return (x + y) ** 0.5

    def __str__(self):
        return f'Line {str(self.first_point), str(self.second_point)} with length {self.length()}'


line1 = Line(point1, point2)

print(line1)
print(line1.length())


line1 = Line(Point(0, 0), Point(0, 5))

print(line1)
print(line1.length())

print('~' * 100)